﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NMock;
using NUnit.Mocks;
using NUnit.Framework;

namespace MockAccountExample
{
    [TestFixture]
    class MockBankAccountTest
    {
        CreditAccount creditAcc;
        BankAccount accountHandler;
        Mock<BankAccount> mockAccount;
        MockFactory factory;

        [SetUp]
        public void initialize() 
        {
    	    factory = new MockFactory();

            //create a Mock<>
            mockAccount = factory.CreateMock<BankAccount>();

			//Assert that the mock is not null
            Assert.IsNotNull(mockAccount, "mock was null");

			//Assert that the mock is not of the interface type
            Assert.IsFalse(typeof(BankAccount).IsInstanceOfType(mockAccount));
        }

        [Test]
        public void test_Balance_Is_Zero()
        {
            CreditAccount acc = new CreditAccount(new BankAccountImpl(0.0));
            Assert.That(acc.getBalance(), NUnit.Framework.Is.EqualTo(0.0));
        }

        [Test]
        public void test_Balance_Increases()
        {
            CreditAccount acc = new CreditAccount(new BankAccountImpl(0.0));
            acc.credit(75);

            Assert.That(acc.getBalance(), NUnit.Framework.Is.EqualTo(75.00));
        }

        [Test]
        public void test_Balance_Decreases()
        {
            CreditAccount acc = new CreditAccount(new BankAccountImpl(100.00));
            acc.debit(35);

            Assert.That(acc.getBalance(), NUnit.Framework.Is.EqualTo(65.00));
        }

        [Test]
        public void test_Balance_Does_Not_Change()
        {
            CreditAccount acc = new CreditAccount(new BankAccountImpl(35.00));
            try
            {
                acc.debit(60);
            }
            catch (InsufficientFunds)
            {
                Assert.That(acc.getBalance(), NUnit.Framework.Is.EqualTo(35.00));
            }
        }
        
        [Test]
        public void fail_Execution_If_Balance_Inadequate() 
        {
            double  balance = 0;

            mockAccount.Expects.One.Method(m => m.getBalance()).WillReturn(0.00);

            accountHandler = mockAccount.MockObject;
            creditAcc = new CreditAccount(accountHandler);

            balance = creditAcc.getBalance();
            Assert.AreEqual(0.0, balance, 0.0);
        }
 
        [Test]
        public void execute_Withdraw_Changes_Balance_And_Sets_Executed() 
        {
            double  balance = 0;
        
            mockAccount.Expects.One.Method(m=>m.getBalance()).WillReturn(2500.00);
            mockAccount.Expects.One.Method(m=>m.getBalance()).WillReturn(2500.00);
            mockAccount.Expects.One.MethodWith(m => m.debit(1500.00)).WillReturn(1000.00);
            // Uncomment the next Expects below to see how things fails, also tinker with CreditAccount.debit()
            // change the line below so it returns 9000.00 and see what happens
            // mockAccount.Expects.One.Method(m => m.getBalance()).WillReturn(1000.00);

            accountHandler = mockAccount.MockObject;
            creditAcc = new CreditAccount(accountHandler);

            balance = creditAcc.getBalance();
            Assert.AreEqual( 2500.00, balance, 0.0 );

            balance = creditAcc.debit(1500.00);
            Assert.AreEqual( 1000.00, balance, 0.0 );
            
            mockAccount.AssertAll();
            //factory.VerifyAllExpectationsHaveBeenMet();
 
            //Assert.AreEqual(true, creditAcc.isExecuted());
        }
 
        [Test]
        public void method_That_We_Expect_Will_Throw_An_Exception() 
        {
            bool expectedThrown = false;
 
            try {
                throw new SystemException();
            } catch (SystemException) {
                expectedThrown = true;
            }
 
            Assert.True(expectedThrown);
        }
    }
}
