﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockAccountExample
{
    public  class CreditAccount : BankAccount
    {
        private BankAccount itsParent;
        public bool executed;    // use this function to prove that certain methods have been called
 
        public  CreditAccount( BankAccount parent )
        {
            itsParent = parent;
        }
 
        public void setAccount(BankAccount parent) 
        {
            itsParent = parent;
        }
 
        public double getBalance()
        {
            return itsParent.getBalance();
        }
 
        public  double   debit( double amt ) 
        {
            double bal = itsParent.getBalance();
            if (bal - amt < 0) 
            {
                throw new InsufficientFunds();
            }
            // Try this version
            /*
            itsParent.debit(amt);
            executed = true;
        
            return itsParent.getBalance();
            */
            
            bal = itsParent.debit(amt);
            executed = true;

            return bal;
        }
    
        public  double  credit( double amt )
        {
            itsParent.credit(amt);
            executed = true;
        
            return itsParent.getBalance();
        }
 
        public bool isExecuted() 
        {
            return executed;
        }
    }
}
