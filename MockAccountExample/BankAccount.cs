﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockAccountExample
{
    public  interface BankAccount
    {
        double getBalance();
        double debit(double amount);
        double credit(double amount);
    }
}
