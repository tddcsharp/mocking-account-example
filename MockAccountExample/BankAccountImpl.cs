﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockAccountExample
{
    public  class BankAccountImpl : BankAccount
    {
        private double itsBalance;

        public BankAccountImpl(double balance)
        {
            itsBalance = balance;
        }

        public double getBalance()
        {
            return itsBalance;
        }

        public double debit(double amount)
        {
            itsBalance -= amount;
            return itsBalance;
        }

        public double credit(double amount)
        {
            itsBalance += amount;
            return itsBalance;
        }
    }
}
